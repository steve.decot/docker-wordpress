# docker-wordpress
Stack Wordpress avec Mariadb
## Révision au 20-02-2022
## Description
Stack combinant wordpress et mariadb. 


## Contenu des stacks de ce repos

### Communs aux stacks :

* Mariadb 10
* Traefik

### one_domain.yml

Cette stack vous permettras de lancer un container wordpress et un container mariadb. 
Le Front répondra sur le nom de domaine que vous aurez défini en remplacant ```${HOSTNAME.DOMAIN.BE}``` par votre domain name. 

##### Exemple

Vous lancez une stack répondant sous le nom de domaine **http://wp.DOMAIN_TLD** . 

* Traefik va capturer la request en HTTP sur le port 80 de l'hote ```- "traefik.http.routers.wordpress-${wp_service_name}.rule=Host(`wp.DOMAIN_TLD`)" ```
* Il va forcer l'HTTPS via ```- "traefik.http.routers.wordpress-${wp_service_name}.entrypoints=websecure"```
* Le container wordpress va s'initialiser mais attendre que celui de mariadb soit up
* Le container de mariadb va s'initialiser avec les logins / passwd / ```${wp_db_name}``` définis dans le compose
* Le front sera prêt pour vos request HTTPS 


#### Stack avec le .env
Variables suivantes sont à affecter:
* ```${wp_service_name}``` : Est le nom de votre service wordpress (identification container, service, network..)
* ```${wp_db_name}``` : Nom de la DB 
* ```${HOSTNAME.DOMAIN.BE}``` : Le hostname que vous définissez


### subdirectories.yml
Cette stack vous permettras de lancer un container wordpress et un container mariadb répondant sous le sub ```${SUBDIR}``` que vous aurez défini.

Le volume ```/var/www/html/${SUBDIR}``` sera monté dans le container et le ```workdir working_dir: /var/www/html/${SUBDIR}``` sera le répertoire par défaut dans le container wordpress. 
##### Exemple

Vous lancez une stack répondant sous le nom de domaine **http://wp.DOMAIN_TLD/worpress_1** . 

* Traefik va capturer la request en HTTP sur le port 80 de l'hote ```- "traefik.http.routers.wordpress-${wp_service_name}.rule=Host(`${wp.DOMAIN_TLD}`)" ``` et sur la la règle ```- "traefik.http.routers.wordpress-${wp_service_name}.rule=PathPrefix(`/wordpress_1{regex:$$|/.*}`)"```. Le container sera donc joignable à travers l'url ```http(s)://wp.DOMAIN_TLD/worpress_1```
* Il va forcer l'HTTPS via ```- "traefik.http.routers.wordpress-${wp_service_name}.entrypoints=websecure"```
* Le container wordpress va s'initialiser mais attendre que celui de mariadb soit up
* Le container de mariadb va s'initialiser avec les logins / passwd / ```${wp_db_name}``` définis dans le compose
* Le front sera prêt pour vos request HTTPS 

#### Variables à modifier
* ```${wp_service_name}``` : Est le nom de votre service wordpress (identification container, service, network..)
* ```${wp_db_name}``` : Nom de la DB 
* ```${SUBDIR}``` : Le sous répertoire présent dans le container WP et dans le nom de domain
* ```${HOSTNAME.DOMAIN.BE}``` : Le hostname que vous définissez
  
## Prochainement

Ajout d'un service wpcli pour automatiser la creation d'un wp
Actuellement le svc_wpcli est fonctionnel mais doit être otpimisé

## Liens
* https://hub.docker.com/_/wordpress
* https://github.com/docker-library/wordpress/issues
* https://stackoverflow.com/questions/58466606/can-t-access-admin-when-wordpress-is-in-a-subfolder
* https://stackoverflow.com/questions/40236713/wordpress-nginx-with-docker-static-files-not-loaded
* https://www.reddit.com/r/Traefik/comments/csmojv/routing_with_wordpress/extrwc9/
